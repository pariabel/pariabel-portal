<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Pariabel | IT Consultant</title>
<meta name="description" content="We are IT consultant. Our focus is to provide technology solutions for your needs.
With us everything is easier." />
<meta name="keywords" content="Pariabeldotcom, Pariabel, IT Solution, Digital Studio, Web Development, Mobile App, Custom Software, Internet Of Things" />

<?php include"css.php"; ?>

</head>
<body>

<?php include"header-alt.php"; ?>

<!-- Products -->
<section class="cd-section bg-img" style="background-image: url('./assets/images/img-product.png');" id="section1" data-width="1024" data-height="768">
	<div class="overlay-img-black"></div>
	<div class="container">
		<div class="box-content">
			<div class="box-table">
				<div class="display-table">
					<div class="display-table-cell">
						<div class="box-content-product">
							<h1> 
								Our Products
							</h1>
						</div>
						<div class="link">
							<a href="#section2">
								<img src="./assets/images/icon-arrow-down.png" class="icon-arrow-down bounce">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Products -->
<section class="cd-section" id="section2">
	<div class="container">
		<div class="box-product">
			<h3>
				Explore <br /> Pariabel Products
			</h3>
			<div class="list-product">
				<div class="video-product">
					<iframe width="300" height="200" src="https://www.youtube.com/embed/n7bYGhEVjd8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
				<small>
					Web & Mobile Application
				</small>
				<h5>
					<a href="detail-product.html" target="_blank">
						Streaming and Video Conference (VC)
					</a>
				</h5>
				<div class="content-product">
					<div class="left-content-product">
						<a href="detail-product.php" target="_blank" class="btn-js-learn-more">
							Learn More	
						</a>
					</div>
					<div class="right-content-product">
						<span class="actDemo btn-js-demo-login">
							Demo
						</span>
					</div>
					<div class="clearer"></div>
				</div>
			</div>
			<div class="list-product">
				<div class="video-product">
					<iframe width="300" height="200" src="https://www.youtube.com/embed/LlhmzVL5bm8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
				<small>
					Hardware Integration
				</small>
				<h5>
					<a href="detail-product.html" target="_blank">
						Device Management System with IOT (Internet of Things)
					</a>
				</h5>
				<div class="content-product">
					<div class="left-content-product">
						<a href="detail-product.php" target="_blank" class="btn-js-learn-more">
							Learn More	
						</a>
					</div>
					<div class="right-content-product">
						<span class="actDemo btn-js-demo-login">
							Demo
						</span>
					</div>
					<div class="clearer"></div>
				</div>
			</div>
			<div class="list-product">
				<div class="video-product">
					<iframe width="300" height="200" src="https://www.youtube.com/embed/R5q_RXk_t7A" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
				<small>
					Software Integration
				</small>
				<h5>
					<a href="detail-product.html" target="_blank">
						Software as a Services <br>(SaaS)
					</a>
				</h5>
				<div class="content-product">
					<div class="left-content-product">
						<a href="detail-product.php" target="_blank" class="btn-js-learn-more">
							Learn More	
						</a>
					</div>
					<div class="right-content-product">
						<span class="actDemo btn-js-demo-login">
							Demo
						</span>
					</div>
					<div class="clearer"></div>
				</div>
			</div>
			<div class="list-product">
				<div class="video-product">
					<iframe width="300" height="200" src="https://www.youtube.com/embed/XVaswAOCuXw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
				<small>
					Desktop , Web & Mobile Application
				</small>
				<h5>
					<a href="detail-product.html" target="_blank">
						Point of Sales (POS) for Custom Business
					</a>
				</h5>
				<div class="content-product">
					<div class="left-content-product">
						<a href="detail-product.php" target="_blank" class="btn-js-learn-more">
							Learn More	
						</a>
					</div>
					<div class="right-content-product">
						<span class="actDemo btn-js-demo-login">
							Demo
						</span>
					</div>
					<div class="clearer"></div>
				</div>
			</div>
			<div class="list-product">
				<div class="video-product">
					<iframe width="300" height="200" src="https://www.youtube.com/embed/c9HfNg4a_Og" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
				<small>
					Web Base Application
				</small>
				<h5>
					<a href="detail-product.html" target="_blank">
						Enterprise Resource Planning Software (ERP)
					</a>
				</h5>
				<div class="content-product">
					<div class="left-content-product">
						<a href="detail-product.php" target="_blank" class="btn-js-learn-more">
							Learn More	
						</a>
					</div>
					<div class="right-content-product">
						<span class="actDemo btn-js-demo-login">
							Demo
						</span>
					</div>
					<div class="clearer"></div>
				</div>
			</div>
			<div class="list-product">
				<div class="video-product">
					<iframe width="300" height="200" src="https://www.youtube.com/embed/hnEQq7kNFWo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
				<small>
					Web Base Application
				</small>
				<h5>
					<a href="detail-product.html" target="_blank">
						Customer Relationship Management Software (CRM)
					</a>
				</h5>
				<div class="content-product">
					<div class="left-content-product">
						<a href="detail-product.php" target="_blank" class="btn-js-learn-more">
							Learn More	
						</a>
					</div>
					<div class="right-content-product">
						<span class="actDemo btn-js-demo-login">
							Demo
						</span>
					</div>
					<div class="clearer"></div>
				</div>
			</div>

			<div class="clearer"></div>
		</div>
	</div>
</section>

<!-- Contact Us -->
<section class="cd-section bg-img" style="background-image: url('./assets/images/img-contact-us.png');" id="section4" data-width="1024" data-height="768">
	<div class="overlay-img-black-alt"></div>
	<div class="container">
		<div class="box-contact">
			<div class="box-table">
				<div class="display-table">
					<div class="display-table-cell">

						<div class="box-connect">
							<h3>Contact Us</h3>
							<div class="left-connect">
								<img src="./assets/images/img-logo.png">
							</div>
							<div class="right-connect">
								<div class="title-connect">
									OFFICE
								</div>
								<div class="desc-connect">
									Puri Nirwana 1 <br />
									Jl. Kalasan Blok Y No. 10 Pabuaran <br />
									Cibinong - Bogor <br />
									Indonesia 
								</div>
								<div class="info-connect">
									<div class="list-info-connect">
										T : 
										<span>
											0858 - 9581 - 0666 
										</span>
									</div>
									<div class="list-info-connect">
										E : 
										<span>
											contact@pariabel.com 
										</span>
									</div>
									<div class="clearer"></div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>



<?php include"footer.php"; ?>

<?php include"modal.php"; ?>

<?php include"js.php"; ?>


</body>
</html> 